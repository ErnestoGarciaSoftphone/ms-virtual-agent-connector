﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using DirectLine = Microsoft.Bot.Connector.DirectLine;

namespace Microsoft.PowerVirtualAgents.Samples.RelayBotSample
{
    /// <summary>
    /// Class for converting Power Virtual Agents bot replied Direct Line Activity responses to standard Bot Schema activities
    /// You can add customized response converting/parsing logic in this class
    /// </summary>
    public class ResponseConverter
    {
        /// <summary>
        /// Convert single DirectLine activity into IMessageActivity instance
        /// </summary>
        /// <returns>IMessageActivity object as a message in a conversation</returns>
        /// <param name="directLineActivity">directline activity</param>
        public IActivity ConvertToBotSchemaActivity(DirectLine.Activity directLineActivity, ITurnContext turnContext)
        {
            if (directLineActivity == null)
            {
                return null;
            }

            if (string.Equals(directLineActivity.Type, DirectLine.ActivityTypes.Event)
                && string.Equals(directLineActivity.Name, "handoff.initiate"))
            {
                Transcript transcript = null;
                // Read transcript from attachment
                if (directLineActivity.Attachments?.Any() == true)
                {
                    DirectLine.Attachment transcriptAttachment = directLineActivity.Attachments.FirstOrDefault(
                        a => string.Equals(a.Name.ToLowerInvariant(), "transcript", System.StringComparison.Ordinal));

                    if (transcriptAttachment != null)
                    {
                        transcript = JsonConvert.DeserializeObject<Transcript>(transcriptAttachment.Content.ToString());
                    }
                }

                // Read handoff context
                HandoffContext handoffContext = JsonConvert.DeserializeObject<HandoffContext>(directLineActivity.Value.ToString());

                return EventFactory.CreateHandoffInitiation(turnContext, handoffContext, transcript);
            }

            var dlAttachments = directLineActivity.Attachments;
            if (dlAttachments != null && dlAttachments.Count() > 0)
            {
                return ConvertToAttachmentActivity(directLineActivity);
            }

            if (directLineActivity.SuggestedActions != null)
            {
                return ConvertToSuggestedActionsAcitivity(directLineActivity);
            }

            if (!string.IsNullOrEmpty(directLineActivity.Text))
            {
                return MessageFactory.Text(directLineActivity.Text);
            }

            return null;
        }

        /// <summary>
        /// Convert a list of DirectLine activities into list of IMessageActivity instances
        /// </summary>
        /// <returns>list of IMessageActivity objects as response messages in a conversation</returns>
        /// <param name="directLineActivities">list of directline activities</param>
        public IList<IActivity> ConvertToBotSchemaActivities(List<DirectLine.Activity> directLineActivities, ITurnContext turnContext)
        {
            return (directLineActivities == null || directLineActivities.Count() == 0) ?
                new List<IActivity>() :
                directLineActivities
                    .Select(directLineActivity => ConvertToBotSchemaActivity(directLineActivity, turnContext))
                    .ToList();
        }

        private IMessageActivity ConvertToAttachmentActivity(DirectLine.Activity directLineActivity)
        {
            var botSchemaAttachments = directLineActivity.Attachments.Select(
                    directLineAttachment => new Attachment()
                    {
                        ContentType = directLineAttachment.ContentType,
                        ContentUrl = directLineAttachment.ContentUrl,
                        Content = directLineAttachment.Content,
                        Name = directLineAttachment.Name,
                        ThumbnailUrl = directLineAttachment.ThumbnailUrl,
                    }).ToList();

            return MessageFactory.Attachment(
                botSchemaAttachments,
                text: directLineActivity.Text,
                ssml: directLineActivity.Speak,
                inputHint: directLineActivity.InputHint);
        }

        private IMessageActivity ConvertToSuggestedActionsAcitivity(DirectLine.Activity directLineActivity)
        {
            var directLineSuggestedActions = directLineActivity.SuggestedActions;
            return MessageFactory.SuggestedActions(
                actions: directLineSuggestedActions.Actions?.Select(action => action.Title).ToList(),
                text: directLineActivity.Text,
                ssml: directLineActivity.Speak,
                inputHint: directLineActivity.InputHint);
        }
    }
}
