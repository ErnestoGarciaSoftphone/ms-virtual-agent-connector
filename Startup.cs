// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.BotBuilderSamples.Bots;
using Microsoft.Extensions.Hosting;
using Softphone.MSVirtualAgentConnector;
using Microsoft.PowerVirtualAgents.Samples.RelayBotSample.Bots;
using Microsoft.PowerVirtualAgents.Samples.RelayBotSample;
using Softphone.MSVirtualAgentConnector.PureCloud;
using Microsoft.Extensions.Logging;
using Softphone.MSVirtualAgentConnector.Handoff;
using Softphone.MSVirtualAgentConnector.BotUtil;

namespace Microsoft.BotBuilderSamples
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            // Create the Bot Framework Adapter with error handling enabled.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();


            // Create the singleton instance of BotService from appsettings
            var botService = new BotService();
            Configuration.Bind("BotService", (object)botService);
            services.AddSingleton<IBotService>(botService);

            // Create the singleton instance of ConversationPool from appsettings
            var conversationManager = new ConversationManager();
            Configuration.Bind("ConversationPool", conversationManager);
            services.AddSingleton(conversationManager);


            var pureCloudGuestChatApiConfiguration = new PureCloudGuestChatApi.Configuration();
            Configuration.Bind("PureCloud", pureCloudGuestChatApiConfiguration);
            services.AddSingleton(pureCloudGuestChatApiConfiguration);

            var botId = Configuration.GetValue<string>("MicrosoftAppId");

            // RelayBot is a singleton, as it manages conversations on its own.
            services.AddSingleton<RelayBot, RelayBot>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            //services.AddSingleton<IBot, IBot>(provider => new HandoffBot(provider.GetService<RelayBot>(), new EchoBot()));

            services.AddSingleton<PureCloudGuestChatApi, PureCloudGuestChatApi>();

            // Just the PureCloud adapter bot
            //services.AddSingleton<IBot, IBot>(provider => new PureCloudBot(
            //    botId, 
            //    new PureCloudGuestChatApi(pureCloudGuestChatApiConfiguration)));

            // Handoff from DirectLine bot to PureCloud bot
            services.AddSingleton<IBot, IBot>(provider =>
                new ConversationRouterBot(turnContext =>
                {
                    var pureCloudBot = new PureCloudBot(
                        botId,
                        provider.GetService<PureCloudGuestChatApi>(),
                        provider.GetService<ILogger<PureCloudBot>>());

                    return new HandoffBot(
                        provider.GetService<RelayBot>(),
                        pureCloudBot,
                        provider.GetService<ILogger<HandoffBot>>());
                },
                provider.GetService<ILogger<ConversationRouterBot>>()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles()
                .UseStaticFiles()
                .UseWebSockets()
                .UseRouting()
                .UseAuthorization()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });

            // app.UseHttpsRedirection();
        }
    }
}
