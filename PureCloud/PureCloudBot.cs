﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Softphone.MSVirtualAgentConnector.Handoff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.PureCloud
{
    public class PureCloudBot : ActivityHandler, HandoffHandler
    {
        readonly ILogger<PureCloudBot> logger;
        readonly string botId;
        readonly PureCloudGuestChatApi pureCloudGuestChatApi;

        ConversationReference conversationReference;
        PureCloudGuestChatApi.Chat pureCloudChat;

        public PureCloudBot(string botId, PureCloudGuestChatApi pureCloudGuestChatApi, ILogger<PureCloudBot> logger)
        {
            this.botId = botId;
            this.pureCloudGuestChatApi = pureCloudGuestChatApi;
            this.logger = logger;
        }

        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default)
        {
            await CreateChatForConversation(turnContext);

            await base.OnTurnAsync(turnContext, cancellationToken);
        }

        async Task CreateChatForConversation(ITurnContext turnContext)
        {
            if (conversationReference == null ||
                conversationReference.Conversation.Id != turnContext.Activity.GetConversationReference().Conversation.Id)
            {
                conversationReference = turnContext.Activity.GetConversationReference();

                var adapter = turnContext.Adapter;

                pureCloudChat = await pureCloudGuestChatApi.StartChat(
                    onMessage: message =>
                    {
                        adapter.ContinueConversationAsync(
                            botId,
                            conversationReference,
                            async (ITurnContext turnContext, CancellationToken cancellationToken) =>
                            {
                                await turnContext.SendActivityAsync(message);
                            },
                            CancellationToken.None);
                    },

                    onTyping: () =>
                    {
                        adapter.ContinueConversationAsync(
                            botId,
                            conversationReference,
                            async (ITurnContext turnContext, CancellationToken cancellationToken) =>
                            {
                                await turnContext.SendActivityAsync(Activity.CreateTypingActivity());
                            },
                            CancellationToken.None);
                    },

                    onAgentJoined: () =>
                    {
                        adapter.ContinueConversationAsync(
                            botId,
                            conversationReference,
                            async (ITurnContext turnContext, CancellationToken cancellationToken) =>
                            {
                                await turnContext.SendActivityAsync("_Agent has joined the conversation_");
                            },
                            CancellationToken.None);
                    },

                    onClosed: () =>
                    {
                        logger.LogDebug("WebSocket disconnected");
                    },

                    onError: exception =>
                        logger.LogWarning("WebSocket error: {Exception}", exception)); ;
            }
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            await base.OnMessageActivityAsync(turnContext, cancellationToken);

            await pureCloudChat.SendMessage(turnContext.Activity.Text);
        }

        protected override async Task OnTypingActivityAsync(ITurnContext<ITypingActivity> turnContext, CancellationToken cancellationToken)
        {
            await base.OnTypingActivityAsync(turnContext, cancellationToken);

            await pureCloudChat.SendTyping();
        }

        public void HandleHandoff(ITurnContext turnContext, Activity handoffActivity)
        {
            var transcriptAttachment = handoffActivity.Attachments.FirstOrDefault(attachment =>
                attachment.Name == "Transcript" &&
                attachment.Content is Transcript);

            if (transcriptAttachment != null)
            {
                var transcript = (Transcript)transcriptAttachment.Content;

                var transcriptLines = transcript.Activities
                    .Where(activity => activity.Type == "message")
                    .Select(activity =>
                    {
                        var line = new StringBuilder();
                        var isBot = activity.From.Role == "bot";
                        //if (!isBot) line.Append("**");
                        //line.Append($"{activity.From.Name}:\t {activity.Text}");
                        line.Append(isBot ? "\U0001F916 " : "\U0001F464 ");
                        line.Append($"{activity.Text}");
                        //if (!isBot) line.Append("**");
                        return line.ToString();
                    });

                var transcriptMessage =
                    "___\n" +
                    "Previous conversation with bot:\n" +
                    "___\n" +
                    string.Join("\n\n", transcriptLines) + "\n" +
                    "___\n";

                _ = CreateChatAndSendTranscript(turnContext, transcriptMessage);
            }
        }

        async Task CreateChatAndSendTranscript(ITurnContext turnContext, string transcriptMessage)
        {
            await CreateChatForConversation(turnContext);
            await pureCloudChat.SendMessage(transcriptMessage);
        }
    }
}
