﻿using Microsoft.Extensions.Logging;
using Microsoft.Rest.Serialization;
using Newtonsoft.Json.Linq;
using Softphone.MSVirtualAgentConnector.Net;
using System;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.PureCloud
{
    public class PureCloudGuestChatApi
    {
        public class Chat
        {
            readonly Configuration configuration;
            readonly HttpClient httpClient;
            readonly string conversationId;
            readonly string memberId;
            readonly string jwt;

            public Chat(Configuration configuration, HttpClient httpClient, 
                ClientWebSocket webSocket, string conversationId, string memberId, string jwt)
            {
                this.configuration = configuration;
                this.httpClient = httpClient;
                WebSocket = webSocket;
                this.conversationId = conversationId;
                this.memberId = memberId;
                this.jwt = jwt;
            }

            public ClientWebSocket WebSocket { get; }

            public async Task CloseAsync()
            {
                await WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, null, CancellationToken.None);
            }

            public async Task SendMessage(string text)
            {
                using var httpRequest = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"https://api.{configuration.Domain}/api/v2/webchat/guest/conversations/{conversationId}/members/{memberId}/messages"),
                    Content = new StringContent(SafeJsonConvert.SerializeObject(new
                    {
                        body = text,
                        bodyType = "standard",
                    }),
                    Encoding.UTF8,
                    "application/json"),
                };

                httpRequest.Headers.Add("Authorization", "Bearer " + jwt);

                await httpClient.SendAsync(httpRequest);
            }

            public async Task SendTyping()
            {
                using var httpRequest = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"https://api.{configuration.Domain}/api/v2/webchat/guest/conversations/{conversationId}/members/{memberId}/typing"),
                };

                httpRequest.Headers.Add("Authorization", "Bearer " + jwt);

                await httpClient.SendAsync(httpRequest);
            }
        }

        readonly ILogger<PureCloudGuestChatApi> logger;

        readonly HttpClient httpClient = new HttpClient();
        readonly Configuration configuration;

        public PureCloudGuestChatApi(Configuration configuration, ILogger<PureCloudGuestChatApi> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
        }

        public async Task<Chat> StartChat(
            Action<string> onMessage,
            Action onTyping,
            Action onAgentJoined,
            Action onClosed,
            Action<Exception> onError)
        {
            using var httpRequest = new HttpRequestMessage
            {
                Method = HttpMethod.Post,

                RequestUri = new Uri($"https://api.{configuration.Domain}/api/v2/webchat/guest/conversations"),

                Content = new StringContent(SafeJsonConvert.SerializeObject(new
                {
                    organizationId = configuration.OrganizationId,
                    deploymentId = configuration.DeploymentId,
                    routingTarget = new
                    {
                        targetType = "QUEUE",
                        targetAddress = configuration.TargetQueueName,
                    },
                    memberInfo = new
                    {
                        displayName = "John Doe",
                        //"profileImageUrl": "https://banner2.kisspng.com/20181201/xes/kisspng-call-centre-customer-service-clip-art-computer-ico-homepage-vacuum-5c02fa4fe698b1.3075985415436990239445.jpg",
                        customFields = new
                        {
                            firstName = "John",
                            lastName = "Doe",
                        },
                    },
                }),
                Encoding.UTF8,
                "application/json"),
            };

            using var httpResponse = await httpClient.SendAsync(httpRequest);
            var responseString = await httpResponse.Content.ReadAsStringAsync();
            var response = JObject.Parse(responseString);

            var webSocket = new ClientWebSocket();
            await webSocket.ConnectAsync(new Uri((string) response["eventStreamUri"]), CancellationToken.None);

            var memberId = (string)response["member"]["id"];
            var onTextHandler = new OnTextHandler(logger);
            var receiver = new WebSocketReceiver(webSocket,
                onText: (message) => onTextHandler.OnText(message, memberId, onMessage, onTyping, onAgentJoined),
                onClosed: onClosed, 
                onError: onError);

            receiver.Start();

            return new Chat(configuration, httpClient, webSocket,
                conversationId: (string)response["id"],
                memberId: memberId,
                jwt: (string)response["jwt"]);
        }

        class OnTextHandler
        {
            readonly ILogger<PureCloudGuestChatApi> logger;
            
            volatile string agentMemberId;
            volatile bool agentJoinedNotified;

            public OnTextHandler(ILogger<PureCloudGuestChatApi> logger)
            {
                this.logger = logger;
            }

            public void OnText(string messageString, string myMemberId, 
                Action<string> onMessage, Action onTyping, Action onAgentJoined)
            {
                var message = JObject.Parse(messageString);

                if (logger.IsEnabled(LogLevel.Trace) && 
                    "channel.metadata" != (string)message["topicName"])
                    logger.LogTrace("Received websocket message: {Message}", message.ToString());

                switch ((string)message["metadata"]?["type"])
                {
                    case "message":
                        switch ((string)message["eventBody"]["bodyType"])
                        {
                            case "standard":
                                if (myMemberId != (string)message["eventBody"]["sender"]["id"])
                                    onMessage((string)message["eventBody"]?["body"]);
                                break;

                            case "member-join":
                                if (agentMemberId != null &&
                                    agentMemberId == (string)message["eventBody"]["sender"]["id"])
                                    onAgentJoined();
                                break;
                        }
                        break;

                    case "typing-indicator":
                        onTyping();
                        break;

                    case "member-change":
                        switch ((string)message["eventBody"]["member"]["state"])
                        {
                            case "ALERTING":
                                // There are 2 parties added to the chat (apart from the client user).
                                // We distinguish the agent because it is the party that goes into 
                                // ALERTING state when receiving the chat interaction.
                                if (agentMemberId == null)
                                    agentMemberId = (string)message["eventBody"]["member"]["id"];
                                break;
                        }
                        break;
                }
            }
        }

        public class Configuration
        {
            public string Domain { get; set; }
            public string OrganizationId { get; set; }
            public string DeploymentId { get; set; }
            public string TargetQueueName { get; set; }
        }
    }
}
