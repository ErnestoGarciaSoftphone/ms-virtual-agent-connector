﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.Net
{
    public class WebSocketReceiver
    {
        const int ReceiveChunkSize = 1024;

        readonly WebSocket webSocket;
        readonly Action<string> onText;
        readonly Action onClosed;
        readonly Action<Exception> onError;

        public WebSocketReceiver(
            WebSocket webSocket,
            Action<string> onText,
            Action onClosed,
            Action<Exception> onError)
        {
            this.webSocket = webSocket;
            this.onText = onText;
            this.onClosed = onClosed;
            this.onError = onError;
        }

        public void Start()
        {
            _ = StartReceivingLoop();
        }

        async Task StartReceivingLoop()
        {
            var buffer = new byte[ReceiveChunkSize];

            try
            {
                while (webSocket.State == WebSocketState.Open)
                {
                    bool endOfMessage = false;
                    while (!endOfMessage)
                    {
                        endOfMessage = await ReceiveMessage(CancellationToken.None);
                    }
                }
            }
            catch (Exception e)
            {
                onError(e);
            }

            onClosed();

            webSocket.Dispose();
        }

        async Task<bool> ReceiveMessage(CancellationToken cancellationToken)
        {
            var buffer = new ArraySegment<byte>(new byte[8192]);
            var stream = new MemoryStream();
            WebSocketReceiveResult data = null;
            do
            {
                data = await webSocket.ReceiveAsync(buffer, cancellationToken);
                stream.Write(buffer.Array, buffer.Offset, data.Count);
            }
            while (!data.EndOfMessage);

            stream.Seek(0, SeekOrigin.Begin);

            if (data.MessageType == WebSocketMessageType.Text)
            {
                using var reader = new StreamReader(stream, Encoding.UTF8);
                onText(reader.ReadToEnd());
            }

            return data.EndOfMessage;
        }
    }
}
