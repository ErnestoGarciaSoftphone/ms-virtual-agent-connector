﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.Handoff
{
    public class HandoffBot : IBot
    {
        private readonly ILogger<HandoffBot> logger;
        private readonly IBot bot1;
        private readonly IBot bot2;

        private bool handoffOccurred = false;

        public HandoffBot(IBot bot1, IBot bot2, ILogger<HandoffBot> logger)
        {
            this.bot1 = bot1;
            this.bot2 = bot2;
            this.logger = logger;
        }

        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default)
        {
            if (handoffOccurred)
            {
                await bot2.OnTurnAsync(turnContext, cancellationToken);
            }
            else
            {
                turnContext.OnSendActivities((ITurnContext turnContext, List<Activity> activities, Func<Task<ResourceResponse[]>> next) =>
                    {
                        var handoffActivity = activities.FirstOrDefault(activity => 
                            activity.Type == ActivityTypes.Event && 
                            activity.Name == "handoff.initiate");

                        if (handoffActivity != null)
                        {
                            handoffOccurred = true;

                            if (bot2 is HandoffHandler b)
                                b.HandleHandoff(turnContext, handoffActivity);
                        }

                        return next();
                    });

                await bot1.OnTurnAsync(turnContext, cancellationToken);
            }
        }
    }
}
