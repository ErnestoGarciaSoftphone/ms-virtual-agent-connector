﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;

namespace Softphone.MSVirtualAgentConnector.Handoff
{
    public interface HandoffHandler
    {
        void HandleHandoff(ITurnContext turnContext, Activity handoffActivity);
    }
}