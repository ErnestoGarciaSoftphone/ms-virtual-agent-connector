﻿using Microsoft.Bot.Builder;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.BotUtil
{
    public class ConversationRouterBot : IBot, IDisposable
    {
        readonly ILogger<ConversationRouterBot> logger;

        readonly Func<ITurnContext, IBot> botFactory;

        readonly Timer expirationTimer;

        class ConversationEntry
        {
            public IBot bot;
            public DateTime lastUsedTime;
        }

        readonly ConcurrentDictionary<string, ConversationEntry> conversations = new ConcurrentDictionary<string, ConversationEntry>();

        public ConversationRouterBot(Func<ITurnContext, IBot> botFactory, 
            ILogger<ConversationRouterBot> logger)
        {
            this.botFactory = botFactory;
            this.logger = logger;

            var expirationCheckPeriod = TimeSpan.FromMinutes(10);
            this.expirationTimer = new Timer(
                _ => RemoveExpiredConversations(),
                state: null,
                dueTime: expirationCheckPeriod.Milliseconds,
                period: expirationCheckPeriod.Milliseconds);
        }

        public void Dispose()
        {
            expirationTimer.Dispose();
        }

        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default)
        {
            var conversation = conversations.GetOrAdd(
                turnContext.Activity.Conversation.Id, 
                _ =>
                {
                    var newConversation = new ConversationEntry();
                    newConversation.bot = botFactory(turnContext);
                    return newConversation;
                });

            conversation.lastUsedTime = DateTime.Now;

            await conversation.bot.OnTurnAsync(turnContext, cancellationToken);
        }

        public void RemoveExpiredConversations()
        {
            logger.LogDebug("Conversation router contains {ConversationCount} conversations", conversations.Count);

            var now = DateTime.Now;
            var expirationTime = TimeSpan.FromHours(1);

            foreach (var entry in conversations)
            {
                if (now >= entry.Value.lastUsedTime + expirationTime)
                    conversations.Remove(entry.Key, out _);
            }
        }
    }
}
