using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softphone.MSVirtualAgentConnector.Handoff;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.Tests
{
    [TestClass]
    public class HandoffTests
    {
        readonly BotAdapter botAdapterMock = new Mock<BotAdapter>().Object;

        [TestMethod]
        public async Task Handoff()
        {
            var bot1 = new Mock<ActivityHandler>(MockBehavior.Strict);
            var bot2 = new Mock<ActivityHandler>();
            var handoffBot = new HandoffBot(bot1.Object, bot2.Object, TestUtil.CreateLogger<HandoffBot>());

            // Bot1 replies with message on first invocation
            bot1.Setup(b => b.OnTurnAsync(It.IsAny<ITurnContext>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Callback((ITurnContext turnContext, CancellationToken _) => {
                    turnContext.SendActivityAsync("dummy reply message");
                });

            var turnContext1 = new TurnContext(botAdapterMock, new Activity { Type = "message" });
            await handoffBot.OnTurnAsync(turnContext1, CancellationToken.None);
            bot1.Verify(bot => bot.OnTurnAsync(turnContext1, CancellationToken.None),
                "bot1 must receive message 1 (before handoff)");

            // Bot1 replies with handoff on second invocation
            bot1.Setup(b => b.OnTurnAsync(It.IsAny<ITurnContext>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Callback((ITurnContext turnContext, CancellationToken _) => {
                    turnContext.SendActivityAsync(EventFactory.CreateHandoffInitiation(turnContext, new object()));
                });

            var turnContext2 = new TurnContext(botAdapterMock, new Activity { Type = "message" });
            await handoffBot.OnTurnAsync(turnContext2, CancellationToken.None);
            bot1.Verify(bot => bot.OnTurnAsync(turnContext2, CancellationToken.None),
                "bot1 must receive message 2 (before handoff)");

            // Bot1 is not expected to be called any more. Cause error on subsequent invocations.
            bot1.Reset();

            var turnContext3 = new TurnContext(botAdapterMock, new Activity { Type = "message" });
            await handoffBot.OnTurnAsync(turnContext3, CancellationToken.None);
            bot2.Verify(bot => bot.OnTurnAsync(turnContext3, CancellationToken.None),
                "bot2 must receive message 3 (after bot1's handoff)");
        }
    }
}
