using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softphone.MSVirtualAgentConnector.PureCloud;
using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Softphone.MSVirtualAgentConnector.Tests
{
    [TestClass]
    public class PureCloudChatManualTest
    {
        [TestMethod]
        public async Task CreateChat()
        {
            var chatApi = new PureCloudGuestChatApi(new PureCloudGuestChatApi.Configuration
            {
                Domain = "mypurecloud.com",
                OrganizationId = "e8fa6d4b-32f3-4699-9d6d-e3512fcbe8a2",
                DeploymentId = "1eac3cfc-9632-432f-865b-5f3b4f63cd7a",
                TargetQueueName = "Chatbot Escalation",
            },
            TestUtil.CreateLogger<PureCloudGuestChatApi>());

            var chat = await chatApi.StartChat(
                onMessage: message =>
                    Trace.WriteLine("PureCloud message received: " + message),

                onTyping: () =>
                    Trace.WriteLine("PureCloud typing received"),

                onAgentJoined: () =>
                     Trace.WriteLine("PureCloud agent joined received"),

                onClosed: () =>
                {
                    Trace.WriteLine("WebSocket disconnected");
                },

                onError: exception =>
                    Trace.TraceWarning("WebSocket error: {0}", exception));

            await Task.Delay(TimeSpan.FromSeconds(5));
            await chat.SendMessage("hola");

            await Task.Delay(TimeSpan.FromSeconds(5));
            await chat.SendMessage("hola otra vez");

            await Task.Delay(TimeSpan.FromMinutes(10));
            await chat.CloseAsync();
        }
    }
}
