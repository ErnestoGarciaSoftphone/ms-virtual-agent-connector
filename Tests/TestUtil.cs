﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Softphone.MSVirtualAgentConnector.Tests
{
    class TestUtil
    {
        public static ILogger<T> CreateLogger<T>()
        {
            return new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider()
                .GetService<ILoggerFactory>()
                .CreateLogger<T>();
        }
    }
}
